CREATE DATABASE FifthPractice;
GO

USE FifthPractice;
GO

CREATE TABLE Posts
(
    ID_Posts  INT PRIMARY KEY IDENTITY (1,1),
    Post_type VARCHAR(50) NOT NULL
);
GO

CREATE TABLE Users
(
    ID_Users    INT PRIMARY KEY IDENTITY (1,1),
    Name        VARCHAR(50) NOT NULL,
    Surname     VARCHAR(50) NOT NULL,
    Middle_name VARCHAR(50) NOT NULL,
);
GO

CREATE TABLE Branch
(
    ID_Branch        INT PRIMARY KEY IDENTITY (1,1),
    Branch_placement VARCHAR(50)
);
GO

CREATE TABLE Subscriptions
(
    ID_Subscriptions INT PRIMARY KEY IDENTITY (1,1),
    Branch_ID        INT,
    FOREIGN KEY (Branch_ID) REFERENCES Branch (ID_Branch),
    Praice           DECIMAL(20, 5),
    Validity_period DATE
);
GO

CREATE TABLE Accounts
(
    ID_Accounts INT PRIMARY KEY IDENTITY (1,1),
    Login       VARCHAR(50) NOT NULL,
    Password    VARCHAR(50) NOT NULL,
    Post_ID     INT,
    FOREIGN KEY (Post_ID) REFERENCES Posts (ID_Posts),
    Users_ID    INT UNIQUE,
    FOREIGN KEY (Users_ID) REFERENCES Users (ID_Users),
    Subscriptions_ID       INT unique,
    FOREIGN KEY (Subscriptions_ID) REFERENCES Subscriptions (ID_Subscriptions)
);
GO

CREATE TABLE Buyer
(
    ID_Buyer                   INT PRIMARY KEY IDENTITY (1,1),
    Passport_series_and_number INT         NOT NULL UNIQUE,
    Name                       VARCHAR(50) NOT NULL,
    Surname                    VARCHAR(50) NOT NULL,
    Middle_name                VARCHAR(50) NOT NULL
);
GO

CREATE TABLE Discount_percentage
(
    ID_discount_percentage INT PRIMARY KEY IDENTITY (1,1),
    Discount_percentage    INT,
    Discount_parameter     VARCHAR(50)
);
GO

CREATE TABLE Orders
(
    ID_Orders              INT PRIMARY KEY IDENTITY (1,1),
    Users_ID               INT,
    FOREIGN KEY (Users_ID) REFERENCES Users (ID_Users),
    Buyer_ID               INT,
    FOREIGN KEY (Buyer_ID) REFERENCES Buyer (ID_Buyer),
    Discount_percentage_ID INT,
    FOREIGN KEY (Discount_percentage_ID) REFERENCES Discount_percentage (ID_Discount_percentage),
    Introduced             DECIMAL(20, 5),
    Date_subscription      DATE
);
GO

CREATE TABLE Service
(
    ID_Service   INT PRIMARY KEY IDENTITY (1,1),
    Service_type VARCHAR(50)
);
GO

CREATE TABLE Services_subscriptions
(
    ID_Services_subscriptions INT PRIMARY KEY IDENTITY (1,1),
    Service_ID                INT,
    FOREIGN KEY (Service_ID) REFERENCES Service (ID_Service),
    Subscriptions_ID          INT,
    FOREIGN KEY (Subscriptions_ID) REFERENCES Subscriptions (ID_Subscriptions)
);
GO

--------------------------------------------ЗАПОЛНЕНИЕ ДАННЫМИ-----------------------------------------
-- Вставка данных в таблицу Branch
INSERT INTO Branch (Branch_placement)
VALUES ('Славянский бульвар'),
       ('Нахимовский проспект'),
       ('Лубянка');
GO

-- Вставка данных в таблицу Subscriptions
INSERT INTO Subscriptions ( Branch_ID, Praice, validity_period)
VALUES (1, 500.00, '09.04.2024'),
       (2, 1000.00, '09.04.2024'),
       (3, 2000.00, '09.04.2024');
GO

-- Вставка данных в таблицу Posts
INSERT INTO Posts (Post_type)
VALUES ('Админ'), ('Кассир'), ('Пользователь');
GO

-- Вставка данных в таблицу Users
INSERT INTO Users (Name, Surname, Middle_name)
VALUES ('Иван', 'Иванов', 'Иванович'),
       ('Петр', 'Петров', 'Петрович'),
       ('Мария', 'Сидорова', 'Петровна');
GO

--Вставка данных в таблицу Accounts
INSERT INTO Accounts (login, password, post_id, users_id, subscriptions_id)
VALUES ('adm', '1', 1, 1, 1),
       ('cashier', '1', 2, 2, 2),
       ('adm', '1', 3, 3, 3);
GO

-- Вставка данных в таблицу Buyer
INSERT INTO Buyer (Passport_series_and_number, Name, Surname, Middle_name)
VALUES (12345670, 'Алексей', 'Кузнецов', 'Сергеевич'),
       (98765210, 'Елена', 'Попова', 'Игоревна'),
       (54367890, 'Мария', 'Сидорова', 'Петровна');
GO

-- Вставка данных в таблицу Discount_percentage
INSERT INTO Discount_percentage (Discount_percentage, Discount_parameter)
VALUES (0, 'Ничего не предъявлено'),
       (10, 'Студеческий билет'),
       (20, 'Постоянный клиент'),
       (30, 'Приглошение от друга');
GO

-- Вставка данных в таблицу Orders
INSERT INTO Orders (Users_ID, Buyer_ID, Discount_percentage_ID, Introduced, Date_subscription)
VALUES (1, 1, 1, 500.00, '2024-04-01'),
       (2, 2, 2, 1000.00, '2024-04-02'),
       (3, 3, 3, 2000.00, '2024-04-03');
GO

-- Вставка данных в таблицу Service
INSERT INTO Service (Service_type)
VALUES ('Бассейн'), ('Тренер'), ('Сауна');
GO

-- Вставка данных в таблицу Services_subscriptions
INSERT INTO Services_subscriptions (Service_ID, Subscriptions_ID)
VALUES (1, 1),
       (2, 2),
       (3, 3);
GO
