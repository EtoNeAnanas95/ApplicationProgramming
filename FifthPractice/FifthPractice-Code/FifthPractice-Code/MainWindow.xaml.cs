﻿using System.Windows;

namespace FifthPractice_Code
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly PineappleFitDBEntities _DB = new PineappleFitDBEntities();

        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Content = new RegistrationPage(this, _DB);
        }
    }
}