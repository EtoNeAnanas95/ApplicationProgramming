//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FifthPractice_Code
{
    using System;
    using System.Collections.Generic;
    
    public partial class Services_subscriptions
    {
        public int ID_Services_subscriptions { get; set; }
        public Nullable<int> Service_ID { get; set; }
        public Nullable<int> Subscriptions_ID { get; set; }
    
        public virtual Service Service { get; set; }
        public virtual Subscriptions Subscriptions { get; set; }
    }
}
