﻿using System.Text.RegularExpressions;
using System.Windows;

namespace FifthPractice_Code
{
    public static class Function
    {
        public static bool ContainsEmojis(string input)
        {
            Regex rgx = new Regex(@"\p{Cs}");

            if (rgx.IsMatch(input))
            {
                MessageBox.Show("Смайлики использовать нельзя", "Ошибка валидации", MessageBoxButton.OK, MessageBoxImage.Error);
                return true;
            }
            return false;
        }
    }
}