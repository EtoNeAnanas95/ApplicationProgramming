﻿#pragma checksum "..\..\ManagerMainPage.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "2CD23FF43663AA0CDC751812466F52F910D7EA643D415D21D0387A657DA76AC7"
//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

using MaterialDesignThemes.Wpf;
using MaterialDesignThemes.Wpf.Converters;
using MaterialDesignThemes.Wpf.Transitions;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace FifthPractice_Code {
    
    
    /// <summary>
    /// ManagerMainPage
    /// </summary>
    public partial class ManagerMainPage : System.Windows.Controls.Page, System.Windows.Markup.IComponentConnector {
        
        
        #line 31 "..\..\ManagerMainPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OrdersButton;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\ManagerMainPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BuyerButton;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\ManagerMainPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Discount_percentageButton;
        
        #line default
        #line hidden
        
        
        #line 34 "..\..\ManagerMainPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Services_subscriptionsButton;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\ManagerMainPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button SubscriptionsButton;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\ManagerMainPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button ServiceButton;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\ManagerMainPage.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Frame MainFrame;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/FifthPractice-Code;component/managermainpage.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\ManagerMainPage.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 28 "..\..\ManagerMainPage.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.ExitButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 2:
            this.OrdersButton = ((System.Windows.Controls.Button)(target));
            
            #line 31 "..\..\ManagerMainPage.xaml"
            this.OrdersButton.Click += new System.Windows.RoutedEventHandler(this.OrdersButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 3:
            this.BuyerButton = ((System.Windows.Controls.Button)(target));
            
            #line 32 "..\..\ManagerMainPage.xaml"
            this.BuyerButton.Click += new System.Windows.RoutedEventHandler(this.BuyerButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 4:
            this.Discount_percentageButton = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\ManagerMainPage.xaml"
            this.Discount_percentageButton.Click += new System.Windows.RoutedEventHandler(this.Discount_percentageButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 5:
            this.Services_subscriptionsButton = ((System.Windows.Controls.Button)(target));
            
            #line 34 "..\..\ManagerMainPage.xaml"
            this.Services_subscriptionsButton.Click += new System.Windows.RoutedEventHandler(this.Services_subscriptionsButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 6:
            this.SubscriptionsButton = ((System.Windows.Controls.Button)(target));
            
            #line 35 "..\..\ManagerMainPage.xaml"
            this.SubscriptionsButton.Click += new System.Windows.RoutedEventHandler(this.SubscriptionsButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 7:
            this.ServiceButton = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\ManagerMainPage.xaml"
            this.ServiceButton.Click += new System.Windows.RoutedEventHandler(this.ServiceButton_OnClick);
            
            #line default
            #line hidden
            return;
            case 8:
            this.MainFrame = ((System.Windows.Controls.Frame)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

