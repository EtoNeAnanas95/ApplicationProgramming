﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using FifthPractice_Code.DbDataGrids;

namespace FifthPractice_Code
{
    public partial class UserMainPage : Page
    {
        private readonly PineappleFitDBEntities _db;
        private readonly Accounts user;
        private readonly MainWindow mainWindow;

        public UserMainPage(MainWindow mainWindow, PineappleFitDBEntities DB, Accounts user)
        {
            InitializeComponent();
            this.user = user;
            _db = DB;
            this.mainWindow = mainWindow;
            mainWindow.Height = 450;
            mainWindow.Width = 750;
        }

        private void BasketButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new UserBasket(_db, user);
        }

        private void Current_subscriptionButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new CurrentSubscriptionPage(_db, user);
        }

        private void ExitButton_OnClick(object sender, RoutedEventArgs e)
        {
            mainWindow.MainFrame.Content = new RegistrationPage(mainWindow, _db);
        }
    }
}