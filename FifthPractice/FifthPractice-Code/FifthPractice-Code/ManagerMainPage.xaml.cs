﻿using System.Windows;
using System.Windows.Controls;
using FifthPractice_Code.ManagerDataGrids;

namespace FifthPractice_Code
{
    public partial class ManagerMainPage : Page
    {
        private readonly PineappleFitDBEntities _db;
        private readonly MainWindow mainWindow;

        public ManagerMainPage(MainWindow mainWindow, PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            this.mainWindow = mainWindow;
            mainWindow.Height = 450;
            mainWindow.Width = 850;
            MainFrame.Content = new OrdersTable(_db);
        }

        private void ExitButton_OnClick(object sender, RoutedEventArgs e)
        {
            mainWindow.MainFrame.Content = new RegistrationPage(mainWindow, _db);
        }

        private void OrdersButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new OrdersTable(_db);
        }

        private void BuyerButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new BuyerTable(_db);
        }

        private void Discount_percentageButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new DiscountPercentagePage(_db);
        }

        private void Services_subscriptionsButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new ServicesSubscriptionsTable(_db);
        }

        private void SubscriptionsButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new SubscriptionsTable(_db);
        }

        private void ServiceButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new ServiceTable(_db);
        }
    }
}