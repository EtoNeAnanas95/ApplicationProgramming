﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using MaterialDesignThemes.Wpf;

namespace FifthPractice_Code
{
    public partial class RegistrationPage : Page
    {
        private readonly PineappleFitDBEntities _db;
        private readonly MainWindow mainWindow;
        private TextBox name = new TextBox();
        private TextBox surname = new TextBox();
        private TextBox middleName = new TextBox();
        private RowDefinition rowDefinition3 = new RowDefinition();
        private RowDefinition rowDefinition4 = new RowDefinition();
        private RowDefinition rowDefinition5 = new RowDefinition();

        public RegistrationPage(MainWindow mainWindow, PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            this.mainWindow = mainWindow;
            mainWindow.Height = 650;
            mainWindow.Width = 450;

            surname.HorizontalAlignment = HorizontalAlignment.Center;
            surname.VerticalAlignment = VerticalAlignment.Center;
            surname.Foreground = new SolidColorBrush(Color.FromRgb(164, 165, 165));
            HintAssist.SetHint(surname, "Surname");
            name.HorizontalAlignment = HorizontalAlignment.Center;
            name.VerticalAlignment = VerticalAlignment.Center;
            name.Foreground = new SolidColorBrush(Color.FromRgb(164, 165, 165));
            HintAssist.SetHint(name, "Name");
            middleName.HorizontalAlignment = HorizontalAlignment.Center;
            middleName.VerticalAlignment = VerticalAlignment.Center;
            middleName.Foreground = new SolidColorBrush(Color.FromRgb(164, 165, 165));
            HintAssist.SetHint(middleName, "Middle name");
            
            Grid.SetRow(name, 3);
            Grid.SetRow(surname, 4);
            Grid.SetRow(middleName, 5);
        }

        private void ButtonReg_OnClick(object sender, RoutedEventArgs e)
        {
            if (ApplyButton.Content.ToString() == "Sign in")
            {
                ApplyButton.Content = "Registration";
                
                RegData.RowDefinitions.Add(rowDefinition3);
                RegData.RowDefinitions.Add(rowDefinition4);
                RegData.RowDefinitions.Add(rowDefinition5);
                
                RegData.Children.Add(name);
                RegData.Children.Add(surname);
                RegData.Children.Add(middleName);
            }
        }

        private void ButtonSigIn_OnClick(object sender, RoutedEventArgs e)
        {
            if (ApplyButton.Content.ToString() == "Registration")
            {
                RegData.RowDefinitions.Remove(rowDefinition3);
                RegData.RowDefinitions.Remove(rowDefinition4);
                RegData.RowDefinitions.Remove(rowDefinition5);
                
                ApplyButton.Content = "Sign in";
                RegData.Children.Remove(name);
                RegData.Children.Remove(surname);
                RegData.Children.Remove(middleName);
            }
        }

        private void ApplyButton_OnClick(object sender, RoutedEventArgs e)
        {
            if (ApplyButton.Content.ToString() == "Sign in")
                try
                {
                    if(Function.ContainsEmojis(Login.Text) || Function.ContainsEmojis(Password.Password)) throw new Exception();
                    var user = _db.Accounts.ToList()
                        .FirstOrDefault(x => x.Login == Login.Text && x.Password == Password.Password);
                    if (user != null)
                        switch (user.Post_ID)
                        {
                            case (int)PostsEnum.Admin:
                                mainWindow.MainFrame.Content = new AdminMainPage(mainWindow, _db);
                                break;
                            case (int)PostsEnum.Cashier:
                                mainWindow.MainFrame.Content = new ManagerMainPage(mainWindow, _db);
                                break;
                            case (int)PostsEnum.User:
                                mainWindow.MainFrame.Content = new UserMainPage(mainWindow, _db, user);
                                break;
                        }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Incorrect data");
                }
            else if (ApplyButton.Content.ToString() == "Registration")
                try
                {
                    if (!_db.Accounts.ToList().Exists(x => x.Login == Login.Text))
                    {
                        if(Function.ContainsEmojis(Login.Text) ||
                           Function.ContainsEmojis(Password.Password) ||
                           Function.ContainsEmojis(name.Text) ||
                           Function.ContainsEmojis(surname.Text) ||
                           Function.ContainsEmojis(middleName.Text)
                           ) throw new Exception();
                        if (name.Text == String.Empty) throw new Exception();
                        if (surname.Text == String.Empty) throw new Exception();
                        if (middleName.Text == String.Empty) throw new Exception();
                        Subscriptions newSubscription = new Subscriptions();
                        newSubscription.Branch_ID = 1;
                        newSubscription.Praice = 0;
                        newSubscription.Validity_period = new DateTime();
                        newSubscription.Validity_period = DateTime.MinValue;

                        int idSub = _db.Subscriptions.Add(newSubscription).ID_Subscriptions;

                        Users newUsers = new Users
                        {
                            Name = name.Text,
                            Surname = surname.Text,
                            Middle_name = middleName.Text
                        };

                        int idUs = _db.Users.Add(newUsers).ID_Users;
                        
                        _db.Accounts.Add(new Accounts
                        {
                            Login = Login.Text,
                            Password = Password.Password,
                            Post_ID = 3,
                            Subscriptions_ID = idSub,
                            Users_ID = idUs
                        });
                        _db.SaveChanges();
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Incorrect data");
                }
        }
    }
}