﻿using System.Windows;
using System.Windows.Controls;
using FifthPractice_Code.DbDataGrids;
using FifthPractice_Code.ManagerDataGrids;

namespace FifthPractice_Code
{
    public partial class AdminMainPage : Page
    {
        private PineappleFitDBEntities _db;
        private MainWindow mainWindow;

        public AdminMainPage(MainWindow mainWindow, PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            this.mainWindow = mainWindow;
            mainWindow.Height = 550;
            mainWindow.Width = 1050;
            MainFrame.Content = new OrdersTable(_db);
        }

        private void ExitButton_OnClick(object sender, RoutedEventArgs e)
        {
            mainWindow.MainFrame.Content = new RegistrationPage(mainWindow, _db);
        }

        private void OrdersButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new OrdersTable(_db);
        }

        private void BuyerButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new BuyerTable(_db);
        }

        private void Discount_percentageButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new DiscountPercentagePage(_db);
        }

        private void Services_subscriptionsButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new ServicesSubscriptionsTable(_db);
        }

        private void SubscriptionsButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new SubscriptionsTable(_db);
        }

        private void ServiceButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new ServiceTable(_db);
        }

        private void BranchButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new BranchTable(_db);
        }

        private void PostButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new PostTable(_db);
        }

        private void AccountsButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new AccountsTable(_db);
        }

        private void UsersButton_OnClick(object sender, RoutedEventArgs e)
        {
            MainFrame.Content = new UserTable(_db);
        }
    }
}