﻿using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;

namespace FifthPractice_Code
{
    public static class JSON
    {
        public static List<T> Deserialize<T>(string path)
        {
            try
            {
                var json = File.ReadAllText(path);
                var obj = JsonConvert.DeserializeObject<List<T>>(json);
                if (obj != null)
                    return obj;
                return new List<T>();
            }
            catch
            {
                return new List<T>();
            }
        }
    }
}