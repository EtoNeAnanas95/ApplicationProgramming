﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.DbDataGrids
{
    public partial class CurrentSubscriptionPage : Page
    {
        private readonly PineappleFitDBEntities _db;
        private readonly Accounts user;
        
        
        public CurrentSubscriptionPage(PineappleFitDBEntities DB, Accounts user)
        {
            InitializeComponent();
            this._db = DB;
            this.user = user;
            Services_ID.ItemsSource = DB.Service.ToList();
            Services_ID.DisplayMemberPath = "Service_type";
            Subscriptions_ID.ItemsSource = DB.Branch.ToList();
            Subscriptions_ID.DisplayMemberPath = "Branch_placement";
            MainGrid.ItemsSource = _db.Services_subscriptions.ToList().Where(x => x.Subscriptions_ID == user.Subscriptions.ID_Subscriptions);
        }
        
        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newService = new Services_subscriptions();
            try
            {
                newService.Service_ID = (Services_ID.SelectedItem as Service).ID_Service;
                
                newService.Subscriptions_ID = user.Subscriptions_ID;
                user.Subscriptions.Branch_ID = (Subscriptions_ID.SelectedItem as Branch).ID_Branch;
                _db.Services_subscriptions.Add(newService);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show( exception + "Incorrect data");
            }
            MainGrid.ItemsSource = _db.Services_subscriptions.ToList().Where(x => x.Subscriptions_ID == user.Subscriptions.ID_Subscriptions);
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                (MainGrid.SelectedItem as Services_subscriptions).Service_ID = (Services_ID.SelectedItem as Service).ID_Service;
                (MainGrid.SelectedItem as Services_subscriptions).Subscriptions_ID = (Subscriptions_ID.SelectedItem as Subscriptions).ID_Subscriptions;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            _db.SaveChanges();
            MainGrid.ItemsSource = _db.Services_subscriptions.ToList().Where(x => x.Subscriptions_ID == user.ID_Accounts);
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Services_subscriptions.Remove(MainGrid.SelectedItem as Services_subscriptions);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Services_subscriptions.ToList().Where(x => x.Subscriptions_ID == user.Subscriptions.ID_Subscriptions);
        }
    }
}