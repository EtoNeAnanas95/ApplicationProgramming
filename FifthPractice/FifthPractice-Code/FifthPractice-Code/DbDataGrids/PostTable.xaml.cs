﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace FifthPractice_Code.DbDataGrids
{
    public partial class PostTable : Page
    {
        private PineappleFitDBEntities _db;
        public PostTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            
            MainGrid.ItemsSource = _db.Posts.ToList();
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newPost = new Posts();
            try
            {
                if (Function.ContainsEmojis(Post_type.Text)) throw new Exception();
                newPost.Post_type = Post_type.Text;
                _db.Posts.Add(newPost);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Posts.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Function.ContainsEmojis(Post_type.Text)) throw new Exception();
                (MainGrid.SelectedItem as Posts).Post_type = Post_type.Text;
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Orders.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Posts.Remove(MainGrid.SelectedItem as Posts);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Posts.ToList();
        }
        
        private void ImportButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var dialog = new CommonOpenFileDialog();
                var result = dialog.ShowDialog();
                if (result == CommonFileDialogResult.Ok)
                {
                    var path = dialog.FileNames.First();
                    var productsList = JSON.Deserialize<Posts>(path);
                    foreach (var item in productsList)
                    {
                        _db.Posts.Add(item);
                        _db.SaveChanges();
                    }

                    MainGrid.ItemsSource = _db.Posts.ToList();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }
        }
    }
}