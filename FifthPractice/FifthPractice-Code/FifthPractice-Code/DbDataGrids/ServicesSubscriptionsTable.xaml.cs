﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.ManagerDataGrids
{
    public partial class ServicesSubscriptionsTable : Page
    {
        private readonly PineappleFitDBEntities _db;

        public ServicesSubscriptionsTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            
            MainGrid.ItemsSource = _db.Services_subscriptions.ToList();

            Service_type.ItemsSource = _db.Service.ToList();
            Service_type.DisplayMemberPath = "Service_type";

            Subscriptions_ID.ItemsSource = _db.Subscriptions.ToList();
            Subscriptions_ID.DisplayMemberPath = "ID_Subscriptions";
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newServices_subscriptions = new Services_subscriptions();
            try
            {
                newServices_subscriptions.Service_ID = (Service_type.SelectedItem as Service).ID_Service;
                newServices_subscriptions.Subscriptions_ID = (Subscriptions_ID.SelectedItem as Subscriptions).ID_Subscriptions;
                _db.Services_subscriptions.Add(newServices_subscriptions);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Services_subscriptions.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                (MainGrid.SelectedItem as Services_subscriptions).Service_ID = (Service_type.SelectedItem as Service).ID_Service;
                (MainGrid.SelectedItem as Services_subscriptions).Subscriptions_ID = (Subscriptions_ID.SelectedItem as Subscriptions).ID_Subscriptions;
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Services_subscriptions.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Services_subscriptions.Remove(MainGrid.SelectedItem as Services_subscriptions);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Services_subscriptions.ToList();
        }
    }
}