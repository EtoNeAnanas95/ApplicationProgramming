﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.ManagerDataGrids
{
    public partial class DiscountPercentagePage : Page
    {
        private readonly PineappleFitDBEntities _db;

        public DiscountPercentagePage(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            MainGrid.ItemsSource = _db.Discount_percentage.ToList();
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newDiscount_percentage = new Discount_percentage();
            try
            {
                if (Function.ContainsEmojis(Discount_parameter.Text)) throw new Exception();
                newDiscount_percentage.Discount_percentage1 = Convert.ToInt32(Discount_percentag.Text);
                newDiscount_percentage.Discount_parameter = Discount_parameter.Text;
                _db.Discount_percentage.Add(newDiscount_percentage);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Discount_percentage.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Function.ContainsEmojis(Discount_parameter.Text)) throw new Exception();
                (MainGrid.SelectedItem as Discount_percentage).Discount_percentage1 =
                    Convert.ToInt32(Discount_percentag.Text);
                (MainGrid.SelectedItem as Discount_percentage).Discount_parameter = Discount_parameter.Text;
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Discount_percentage.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Discount_percentage.Remove(MainGrid.SelectedItem as Discount_percentage);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Discount_percentage.ToList();
        }
    }
}