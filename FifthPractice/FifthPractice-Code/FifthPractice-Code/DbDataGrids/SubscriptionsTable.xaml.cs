﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.ManagerDataGrids
{
    public partial class SubscriptionsTable : Page
    {
        private readonly PineappleFitDBEntities _db;

        public SubscriptionsTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            
            MainGrid.ItemsSource = _db.Subscriptions.ToList();
            
            Branch_placement.ItemsSource = _db.Branch.ToList();
            Branch_placement.DisplayMemberPath = "Branch_placement";
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newSubscriptions = new Subscriptions();
            try
            {
                newSubscriptions.Branch_ID = (Branch_placement.SelectedItem as Branch).ID_Branch;
                newSubscriptions.Praice = Convert.ToDecimal(Praice.Text);
                newSubscriptions.Validity_period = Convert.ToDateTime(Validity_period.Text);
                _db.Subscriptions.Add(newSubscriptions);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Subscriptions.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                (MainGrid.SelectedItem as Subscriptions).Branch_ID =
                    (Branch_placement.SelectedItem as Branch).ID_Branch;
                (MainGrid.SelectedItem as Subscriptions).Praice = Convert.ToDecimal(Praice.Text);
                (MainGrid.SelectedItem as Subscriptions).Validity_period = Convert.ToDateTime(Validity_period.Text);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Subscriptions.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Subscriptions.Remove(MainGrid.SelectedItem as Subscriptions);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Subscriptions.ToList();
        }
    }
}