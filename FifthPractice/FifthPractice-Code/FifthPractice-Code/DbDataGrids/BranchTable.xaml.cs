﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace FifthPractice_Code.DbDataGrids
{
    public partial class BranchTable : Page
    {
        private PineappleFitDBEntities _db;
        public BranchTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            MainGrid.ItemsSource = _db.Branch.ToList();
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newBranch = new Branch();
            try
            {
                if (!Function.ContainsEmojis(Branch_placement.Text)) throw new Exception();
                newBranch.Branch_placement = Branch_placement.Text;
                _db.Branch.Add(newBranch);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Orders.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!Function.ContainsEmojis(Branch_placement.Text)) throw new Exception();
                (MainGrid.SelectedItem as Branch).Branch_placement = Branch_placement.Text;
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Orders.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Branch.Remove(MainGrid.SelectedItem as Branch);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Branch.ToList();
        }

        private void ImportButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            var result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok)
            {
                var path = dialog.FileNames.First();
                var productsList = JSON.Deserialize<Branch>(path);
                foreach (var item in productsList)
                {
                    _db.Branch.Add(item);
                    _db.SaveChanges();
                }

                MainGrid.ItemsSource = _db.Branch.ToList();
            }
        }
    }
}