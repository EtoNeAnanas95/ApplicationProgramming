﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.DbDataGrids
{
    public partial class AccountsTable : Page
    {
        private PineappleFitDBEntities _db;

        public AccountsTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;

            MainGrid.ItemsSource = _db.Accounts.ToList();

            Post_type.ItemsSource = _db.Posts.ToList();
            Post_type.DisplayMemberPath = "Post_type";

            ID_Subscriptions.ItemsSource = _db.Subscriptions.ToList();
            ID_Subscriptions.DisplayMemberPath = "ID_Subscriptions";

            Name.ItemsSource = _db.Users.ToList();
            Name.DisplayMemberPath = "Name";
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newAccount = new Accounts();
            try
            {
                if (Function.ContainsEmojis(Login.Text) ||
                    Function.ContainsEmojis(Password.Text)) throw new Exception();
                newAccount.Login = Login.Text;
                newAccount.Password = Password.Text;
                newAccount.Post_ID = (Post_type.SelectedItem as Posts).ID_Posts;
                newAccount.Subscriptions_ID = (ID_Subscriptions.SelectedItem as Subscriptions).ID_Subscriptions;
                newAccount.Users_ID = (Name.SelectedItem as Users).ID_Users;
                _db.Accounts.Add(newAccount);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
                MessageBox.Show(exception.ToString());
            }

            MainGrid.ItemsSource = _db.Accounts.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Function.ContainsEmojis(Login.Text) ||
                    Function.ContainsEmojis(Password.Text)) throw new Exception();
                (MainGrid.SelectedItem as Accounts).Login = Login.Text;
                (MainGrid.SelectedItem as Accounts).Password = Password.Text;
                (MainGrid.SelectedItem as Accounts).Post_ID = (Post_type.SelectedItem as Posts).ID_Posts;
                (MainGrid.SelectedItem as Accounts).Subscriptions_ID =
                    (ID_Subscriptions.SelectedItem as Subscriptions).ID_Subscriptions;
                (MainGrid.SelectedItem as Accounts).Users_ID = (Name.SelectedItem as Users).ID_Users;
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Accounts.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Accounts.Remove(MainGrid.SelectedItem as Accounts);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Accounts.ToList();
        }
    }
}