﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.DbDataGrids
{
    public partial class UserTable : Page
    {
        private PineappleFitDBEntities _db;
        
        public UserTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            
            _db = DB;

            MainGrid.ItemsSource = _db.Users.ToList();
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newUser = new Users();
            try
            {
                if (Function.ContainsEmojis(Name.Text) ||
                    Function.ContainsEmojis(Surname.Text) ||
                    Function.ContainsEmojis(Middle_name.Text)
                    ) throw new Exception();
                newUser.Name = Name.Text;
                newUser.Surname = Surname.Text;
                newUser.Middle_name = Middle_name.Text;
                _db.Users.Add(newUser);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
                MessageBox.Show(exception.ToString());
            }

            MainGrid.ItemsSource = _db.Users.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Function.ContainsEmojis(Name.Text) ||
                    Function.ContainsEmojis(Surname.Text) ||
                    Function.ContainsEmojis(Middle_name.Text)
                   ) throw new Exception();
                (MainGrid.SelectedItem as Users).Name = Name.Text;
                (MainGrid.SelectedItem as Users).Surname = Surname.Text;
                (MainGrid.SelectedItem as Users).Middle_name = Middle_name.Text;
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Users.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Users.Remove(MainGrid.SelectedItem as Users);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Users.ToList();
        }
    }
}