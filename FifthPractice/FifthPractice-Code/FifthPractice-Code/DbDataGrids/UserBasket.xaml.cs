﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.DbDataGrids
{
    public partial class UserBasket : Page
    {
        private readonly PineappleFitDBEntities _db;
        private readonly Accounts user;
        private Orders newOrder;
        
        public UserBasket(PineappleFitDBEntities DB, Accounts user)
        {
            InitializeComponent();
            this._db = DB;
            this.user = user;

            this.newOrder = new Orders();
            this.newOrder.Users_ID = user.Users_ID;
            this.newOrder.Buyer_ID = user.Users_ID;
            this.newOrder.Buyer = new Buyer();
            this.newOrder.Buyer.Name = Name.Text;
            this.newOrder.Buyer.Surname = Surname.Text;
            this.newOrder.Buyer.Middle_name = Middle_name.Text;
            this.newOrder.Discount_percentage_ID = 1;
            this.newOrder.Introduced = 0;
            this.newOrder.Date_subscription = DateTime.Today.AddMonths(1);
        }


        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if(Function.ContainsEmojis(Name.Text) ||
                   Function.ContainsEmojis(Surname.Text) ||
                   Function.ContainsEmojis(Middle_name.Text)
                   ) throw new Exception();
                if (Passport_series_and_number.Text.Length < 8) throw new Exception();
                this.newOrder.Buyer.Passport_series_and_number = Convert.ToInt32(Passport_series_and_number.Text);
                this.newOrder.Buyer.Name = Name.Text;
                this.newOrder.Buyer.Surname = Surname.Text;
                this.newOrder.Buyer.Middle_name = Middle_name.Text;
                _db.Buyer.Add(this.newOrder.Buyer);
                _db.SaveChanges();
                newOrder.Buyer.ID_Buyer = _db.Buyer.ToList().Find(x => x.Passport_series_and_number == newOrder.Buyer.Passport_series_and_number).ID_Buyer;
                _db.Orders.Add(newOrder);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }
        }
    }
}