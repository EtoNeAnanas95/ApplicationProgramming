﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.ManagerDataGrids
{
    public partial class OrdersTable : Page
    {
        private readonly PineappleFitDBEntities _db;

        public OrdersTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            
            MainGrid.ItemsSource = _db.Orders.ToList();

            User_ID.ItemsSource = _db.Users.ToList();
            User_ID.DisplayMemberPath = "Name";

            Buyer_ID.ItemsSource = _db.Buyer.ToList();
            Buyer_ID.DisplayMemberPath = "Name";

            Discount_percentage_ID.ItemsSource = _db.Discount_percentage.ToList();
            Discount_percentage_ID.DisplayMemberPath = "Discount_parameter";
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newOrder = new Orders();
            try
            {
                newOrder.Users_ID = (User_ID.SelectedItem as Users).ID_Users;
                newOrder.Buyer_ID = (Buyer_ID.SelectedItem as Buyer).ID_Buyer;
                newOrder.Discount_percentage_ID = (Discount_percentage_ID.SelectedItem as Discount_percentage).ID_discount_percentage;
                newOrder.Introduced = Convert.ToDecimal(Introduced.Text);
                newOrder.Date_subscription = Convert.ToDateTime(Date_subscription.Text);
                _db.Orders.Add(newOrder);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
                MessageBox.Show(exception.ToString());
            }

            MainGrid.ItemsSource = _db.Orders.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                (MainGrid.SelectedItem as Orders).Users_ID = (User_ID.SelectedItem as Users).ID_Users;
                (MainGrid.SelectedItem as Orders).Buyer_ID = (Buyer_ID.SelectedItem as Buyer).ID_Buyer;
                (MainGrid.SelectedItem as Orders).Discount_percentage_ID = (Discount_percentage_ID.SelectedItem as Discount_percentage).ID_discount_percentage;
                (MainGrid.SelectedItem as Orders).Introduced = Convert.ToDecimal(Introduced.Text);
                (MainGrid.SelectedItem as Orders).Date_subscription = Convert.ToDateTime(Date_subscription.Text);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Orders.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Orders.Remove(MainGrid.SelectedItem as Orders);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Orders.ToList();
        }
    }
}