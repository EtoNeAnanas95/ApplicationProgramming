﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FifthPractice_Code.ManagerDataGrids
{
    public partial class BuyerTable : Page
    {
        private readonly PineappleFitDBEntities _db;

        public BuyerTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;
            MainGrid.ItemsSource = _db.Buyer.ToList();
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newBuyer = new Buyer();
            try
            {
                if (Function.ContainsEmojis(Name.Text) ||
                    Function.ContainsEmojis(Surname.Text) ||
                    Function.ContainsEmojis(Middle_name.Text)) throw new Exception();
                newBuyer.Passport_series_and_number = Convert.ToInt32(Passport_series_and_number.Text);
                newBuyer.Name = Name.Text;
                newBuyer.Surname = Surname.Text;
                newBuyer.Middle_name = Middle_name.Text;
                _db.Buyer.Add(newBuyer);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Buyer.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Function.ContainsEmojis(Name.Text) ||
                    Function.ContainsEmojis(Surname.Text) ||
                    Function.ContainsEmojis(Middle_name.Text)) throw new Exception();
                (MainGrid.SelectedItem as Buyer).Passport_series_and_number = Convert.ToInt32(Passport_series_and_number.Text);
                (MainGrid.SelectedItem as Buyer).Name = Name.Text;
                (MainGrid.SelectedItem as Buyer).Surname = Surname.Text;
                (MainGrid.SelectedItem as Buyer).Middle_name = Middle_name.Text;
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Buyer.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Buyer.Remove(MainGrid.SelectedItem as Buyer);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Buyer.ToList();
        }
    }
}