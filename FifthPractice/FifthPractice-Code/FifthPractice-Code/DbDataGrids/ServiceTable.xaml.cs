﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace FifthPractice_Code.ManagerDataGrids
{
    public partial class ServiceTable : Page
    {
        private readonly PineappleFitDBEntities _db;

        public ServiceTable(PineappleFitDBEntities DB)
        {
            InitializeComponent();
            _db = DB;

            MainGrid.ItemsSource = _db.Service.ToList();
        }

        private void NewButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var newService = new Service();
            try
            {
                if (Function.ContainsEmojis(Service_type.Text)) throw new Exception();
                newService.Service_type = Service_type.Text;
                _db.Service.Add(newService);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Service.ToList();
        }

        private void EditButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Function.ContainsEmojis(Service_type.Text)) throw new Exception();
                (MainGrid.SelectedItem as Service).Service_type = Service_type.Text;
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Service.ToList();
        }

        private void DeleteButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _db.Service.Remove(MainGrid.SelectedItem as Service);
                _db.SaveChanges();
            }
            catch (Exception exception)
            {
                MessageBox.Show("Incorrect data");
            }

            MainGrid.ItemsSource = _db.Service.ToList();
        }
        
        private void ImportButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog();
            var result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok)
            {
                var path = dialog.FileNames.First();
                var productsList = JSON.Deserialize<Service>(path);
                foreach (var item in productsList)
                {
                    _db.Service.Add(item);
                    _db.SaveChanges();
                }

                MainGrid.ItemsSource = _db.Service.ToList();
            }
        }
    }
}