﻿using System.Windows.Controls;
using FirstPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace FirstPractice_DataSet
{
    public partial class Wages : Page
    {
        public Wages(WagesTableAdapter _wages)
        {
            InitializeComponent();
            MainDataGrid.ItemsSource = _wages.GetData();
        }
    }
}