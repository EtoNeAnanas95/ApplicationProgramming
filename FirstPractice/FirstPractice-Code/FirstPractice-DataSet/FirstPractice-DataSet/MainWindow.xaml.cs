﻿using System.Windows;
using FirstPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace FirstPractice_DataSet
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        private readonly AccountingTableAdapter _accounting = new AccountingTableAdapter();
        private readonly SalaryTableAdapter _salary = new SalaryTableAdapter();
        private readonly WagesTableAdapter _wages = new WagesTableAdapter();

        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Content = new Accounting(_accounting);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (MainFrame.Content is Salary)
                MainFrame.Content = new Wages(_wages);
            else if (MainFrame.Content is Wages)
                MainFrame.Content = new Accounting(_accounting);
            else
                MainFrame.Content = new Salary(_salary);
        }
    }
}