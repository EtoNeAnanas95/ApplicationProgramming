﻿using System.Windows.Controls;
using FirstPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace FirstPractice_DataSet
{
    public partial class Accounting : Page
    {
        public Accounting(AccountingTableAdapter _accounting)
        {
            InitializeComponent();
            MainDataGrid.ItemsSource = _accounting.GetData();
        }
    }
}