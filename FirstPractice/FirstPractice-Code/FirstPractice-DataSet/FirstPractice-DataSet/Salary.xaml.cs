﻿using System.Windows.Controls;
using FirstPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace FirstPractice_DataSet
{
    public partial class Salary : Page
    {
        public Salary(SalaryTableAdapter _salary)
        {
            InitializeComponent();
            MainDataGrid.ItemsSource = _salary.GetData();
        }
    }
}