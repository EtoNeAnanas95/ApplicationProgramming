﻿using System.Data.Entity;
using System.Linq;
using System.Windows.Controls;

namespace FirstPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для SalaryPage.xaml
    /// </summary>
    public partial class SalaryPage : Page
    {
        public SalaryPage(DbSet<Salary> salary)
        {
            InitializeComponent();
            MainDataGrid.ItemsSource = salary.ToList();
        }
    }
}