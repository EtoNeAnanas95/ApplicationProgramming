﻿using System.Data.Entity;
using System.Linq;
using System.Windows.Controls;

namespace FirstPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для WagesPage.xaml
    /// </summary>
    public partial class WagesPage : Page
    {
        public WagesPage(DbSet<Wages> wages)
        {
            InitializeComponent();
            MainDataGrid.ItemsSource = wages.ToList();
        }
    }
}