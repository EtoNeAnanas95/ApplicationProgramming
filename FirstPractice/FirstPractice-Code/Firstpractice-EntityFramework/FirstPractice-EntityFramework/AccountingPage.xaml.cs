﻿using System.Data.Entity;
using System.Linq;
using System.Windows.Controls;

namespace FirstPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для AccountingPage.xaml
    /// </summary>
    public partial class AccountingPage : Page
    {
        public AccountingPage(DbSet<Accounting> accounting)
        {
            InitializeComponent();
            MainDataGrid.ItemsSource = accounting.ToList();
        }
    }
}