﻿using System.Windows;

namespace FirstPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly FirstPracticeEntities1 DB = new FirstPracticeEntities1();

        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Content = new AccountingPage(DB.Accounting);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (MainFrame.Content is AccountingPage)
                MainFrame.Content = new WagesPage(DB.Wages);
            else if (MainFrame.Content is WagesPage)
                MainFrame.Content = new SalaryPage(DB.Salary);
            else
                MainFrame.Content = new AccountingPage(DB.Accounting);
        }
    }
}