CREATE DATABASE FirstPractice;
GO

USE FirstPractice;
GO

CREATE TABLE Salary
(
    ID_salary INT PRIMARY KEY IDENTITY (1,1),
    salary    int         NOT NULL,
    post      varchar(25) NOT NULL
);
GO

CREATE TABLE Wages
(
    ID_wages  INT PRIMARY KEY IDENTITY (1,1),
    wage      int,
    salary_ID int,
    FOREIGN KEY (salary_ID) REFERENCES Salary (ID_salary)
);
GO

CREATE TABLE Accounting
(
    ID_accounting      INT PRIMARY KEY IDENTITY (1,1),
    wages_payment_date date        NOT NULL,
    name               varchar(30) NOT NULL,
    surname            varchar(30) NOT NULL,
    middle_name        varchar(30),
    wages_ID           int UNIQUE,
    FOREIGN KEY (wages_ID) REFERENCES Wages (ID_wages)
);
GO

INSERT INTO Salary(SALARY, POST)
VALUES (300000, 'SENIOR DEVELOPER'),
       (200000, 'MIDDLE DEVELOPER'),
       (100000, 'JUNIOR DEVELOPER');
GO

INSERT INTO Wages(WAGE, SALARY_ID)
VALUES ((SELECT salary FROM Salary WHERE ID_salary = 1), 1),
       ((SELECT salary FROM Salary WHERE ID_salary = 2), 2),
       ((SELECT salary FROM Salary WHERE ID_salary = 3), 3);
GO

INSERT INTO Accounting (WAGES_PAYMENT_DATE, NAME, SURNAME, MIDDLE_NAME, WAGES_ID)
VALUES ('2023-03-15', 'John', 'Doe', 'Smith', 1),
       ('2022-12-20', 'Jane', 'Doe', 'Williams', 2),
       ('2024-01-05', 'Alice', 'Johnson', 'Brown', 3);
GO