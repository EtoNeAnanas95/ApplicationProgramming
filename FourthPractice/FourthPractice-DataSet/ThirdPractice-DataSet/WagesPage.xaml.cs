﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ThirdPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace ThirdPractice_DataSet
{
    public partial class WagesPage : Page
    {
        private readonly WagesTableAdapter _wages = new WagesTableAdapter();
        private readonly SalaryTableAdapter _salary = new SalaryTableAdapter();
        public WagesPage()
        {
            InitializeComponent();
            FitlerComboBox.ItemsSource = _salary.GetData();
            FitlerComboBox.DisplayMemberPath = "salary";
            MainGrid.ItemsSource = _wages.GetFullData();
        }

        private void SearchTextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                MainGrid.ItemsSource = null;
                MainGrid.ItemsSource = _wages.GetDataBySearch(SearchTextBox.Text);
            }
        }

        private void FitlerComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FitlerComboBox.SelectedItem != null)
            {
                MainGrid.ItemsSource = null;
                int id = Convert.ToInt32((FitlerComboBox.SelectedItem as DataRowView).Row[0]);
                MainGrid.ItemsSource = _wages.GetDataByFilter(id);
            }
        }

        private void ClearDataGrid(object sender, RoutedEventArgs e)
        {
            MainGrid.ItemsSource = null;
            MainGrid.ItemsSource = _wages.GetFullData();
        }
    }
}