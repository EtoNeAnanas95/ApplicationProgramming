﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using ThirdPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace ThirdPractice_DataSet
{
    public partial class AccountingPage : Page
    {
        private readonly AccountingTableAdapter _accounting = new AccountingTableAdapter();

        private readonly SearchClass[] _searchClass = new SearchClass[3];

        public AccountingPage()
        {
            InitializeComponent();
            string[] txt = { "Post", "Surname", "Name" };
            for (var i = 0; i < txt.Length; i++)
            {
                _searchClass[i] = new SearchClass();
                _searchClass[i].DisplayValue = txt[i];
                _searchClass[i]._value = string.Empty;
            }

            SearchComboBox.ItemsSource = _searchClass;
            SearchComboBox.DisplayMemberPath = "DisplayValue";

            SalaryTableAdapter salary = new SalaryTableAdapter();
            FitlerComboBox.ItemsSource = salary.GetData();
            FitlerComboBox.DisplayMemberPath = "post";
            
            MainGrid.ItemsSource = _accounting.GetFullInfo();
        }

        private void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            MainGrid.Columns[0].Visibility = Visibility.Hidden;
            MainGrid.Columns[5].Visibility = Visibility.Hidden;
            MainGrid.Columns[6].Visibility = Visibility.Hidden;
            MainGrid.Columns[8].Visibility = Visibility.Hidden;
            MainGrid.Columns[9].Visibility = Visibility.Hidden;

            MainGrid.Columns[1].Header = "Дата выдачи зп";
            MainGrid.Columns[2].Header = "Имя";
            MainGrid.Columns[3].Header = "Фамилия";
            MainGrid.Columns[4].Header = "Отчество";
            MainGrid.Columns[7].Header = "ЗП";
            MainGrid.Columns[10].Header = "Оклад";
            MainGrid.Columns[11].Header = "Должность";
        }

        private void SearchTextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                (SearchComboBox.SelectedItem as SearchClass)._value = SearchTextBox.Text;
                MainGrid.ItemsSource = null;
                MainGrid.ItemsSource = _accounting.SearchData(_searchClass[0]._value, _searchClass[1]._value,
                    _searchClass[2]._value);
                MainPage_OnLoaded(null, new RoutedEventArgs());
            }
        }

        private void ClearDataGrid(object sender, RoutedEventArgs e)
        {
            MainGrid.ItemsSource = null;
            MainGrid.ItemsSource = _accounting.GetFullInfo();
        }

        private void FitlerComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FitlerComboBox.SelectedItem != null)
            {
                MainGrid.ItemsSource = null;
                int id = Convert.ToInt32((FitlerComboBox.SelectedItem as DataRowView).Row[0]);
                MainGrid.ItemsSource = _accounting.FilterByPost(id);
            }
        }
    }
}