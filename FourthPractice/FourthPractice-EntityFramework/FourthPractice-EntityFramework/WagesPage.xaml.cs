﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FourthPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для WagesPage.xaml
    /// </summary>
    public partial class WagesPage : Page
    {
        private readonly FirstPracticeEntities DB = new FirstPracticeEntities();
        public WagesPage()
        {
            InitializeComponent();
            MainGrid.ItemsSource = DB.Wages.ToList();
            FitlerComboBox.ItemsSource = DB.Salary.ToList();
            FitlerComboBox.DisplayMemberPath = "salary1";
        }

        private void SearchTextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    MainGrid.ItemsSource = DB.Wages.ToList().Where(item => item.wage.ToString().Contains(SearchTextBox.Text));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void FitlerComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FitlerComboBox.SelectedItem != null)
            {
                var select = FitlerComboBox.SelectedItem as Salary;
                MainGrid.ItemsSource = DB.Wages.ToList().Where(item => item.Salary.ID_salary == select.ID_salary);
            }
        }

        private void ClearDataGrid(object sender, RoutedEventArgs e)
        {
            MainGrid.ItemsSource = null;
            MainGrid.ItemsSource = DB.Wages.ToList();
        }
    }
}