﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FourthPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для AccountingPage.xaml
    /// </summary>
    public partial class AccountingPage : Page
    {
        private readonly FirstPracticeEntities DB = new FirstPracticeEntities();
        public AccountingPage()
        {
            InitializeComponent();
            MainGrid.ItemsSource = DB.Accounting.ToList();
            FitlerComboBox.ItemsSource = DB.Salary.ToList();
            FitlerComboBox.DisplayMemberPath = "post";
        }

        private void SearchTextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    MainGrid.ItemsSource = DB.Accounting.ToList().Where(item => item.name.Contains(SearchTextBox.Text));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void FitlerComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FitlerComboBox.SelectedItem != null)
            {
                var select = FitlerComboBox.SelectedItem as Salary;
                MainGrid.ItemsSource = DB.Accounting.ToList().Where(item => item.Wages.Salary.ID_salary == select.ID_salary);
            }
        }

        private void ClearDataGrid(object sender, RoutedEventArgs e)
        {
            MainGrid.ItemsSource = null;
            MainGrid.ItemsSource = DB.Accounting.ToList();
        }
    }
}