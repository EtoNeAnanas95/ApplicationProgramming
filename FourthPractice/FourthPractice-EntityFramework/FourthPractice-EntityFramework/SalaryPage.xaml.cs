﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace FourthPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для SalaryPage.xaml
    /// </summary>
    public partial class SalaryPage : Page
    {
        private readonly FirstPracticeEntities DB = new FirstPracticeEntities();
        
        public SalaryPage()
        {
            InitializeComponent();
            MainGrid.ItemsSource = DB.Salary.ToList();
            FitlerComboBox.ItemsSource = DB.Salary.ToList();
            FitlerComboBox.DisplayMemberPath = "salary1";
        }

        private void SearchTextBox_OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
                    MainGrid.ItemsSource = DB.Salary.ToList().Where(item => item.post.Contains(SearchTextBox.Text));
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void FitlerComboBox_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (FitlerComboBox.SelectedItem != null)
            {
                var select = FitlerComboBox.SelectedItem as Salary;
                MainGrid.ItemsSource = DB.Salary.ToList().Where(item => item.ID_salary == select.ID_salary);
            }
        }

        private void ClearDataGrid(object sender, RoutedEventArgs e)
        {
            MainGrid.ItemsSource = null;
            MainGrid.ItemsSource = DB.Salary.ToList();
        }
    }
}