﻿using System.Windows;

namespace FourthPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Content = new AccountingPage();
        }

        private void GoNextTable_Click(object sender, RoutedEventArgs e)
        {
            if (MainFrame.Content is AccountingPage)
                MainFrame.Content = new WagesPage();
            else if (MainFrame.Content is WagesPage)
                MainFrame.Content = new SalaryPage();
            else if (MainFrame.Content is SalaryPage) MainFrame.Content = new AccountingPage();
        }
    }
}