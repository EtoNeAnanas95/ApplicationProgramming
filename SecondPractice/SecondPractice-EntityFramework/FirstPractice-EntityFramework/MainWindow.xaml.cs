﻿using System.Windows;

namespace FirstPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly FirstPracticeEntities DB = new FirstPracticeEntities();

        public MainWindow()
        {
            InitializeComponent();
            MainFrame.Content = new AccountingPage(DB);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (MainFrame.Content is AccountingPage)
                MainFrame.Content = new WagesPage(DB);
            else if (MainFrame.Content is WagesPage)
                MainFrame.Content = new SalaryPage(DB);
            else
                MainFrame.Content = new AccountingPage(DB);
        }
    }
}