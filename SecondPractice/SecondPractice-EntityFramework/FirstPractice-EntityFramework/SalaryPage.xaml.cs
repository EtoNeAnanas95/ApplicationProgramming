﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FirstPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для SalaryPage.xaml
    /// </summary>
    public partial class SalaryPage : Page
    {
        private readonly FirstPracticeEntities DB;

        public SalaryPage(FirstPracticeEntities DB)
        {
            InitializeComponent();
            this.DB = DB;
            MainDataGrid.ItemsSource = DB.Salary.ToList();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                var s = new Salary();
                s.salary1 = Convert.ToInt32(SalaryTextBox.Text);
                s.post = PostTextBox.Text;
                message = "You are add new salary";
                DB.Salary.Add(s);
                DB.SaveChanges();
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }

            MainDataGrid.ItemsSource = DB.Salary.ToList();
            MessageBox.Show(message);
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                message = "You are add new wage";
                var selected = MainDataGrid.SelectedItem as Salary;
                selected.salary1 = Convert.ToInt32(SalaryTextBox.Text);
                selected.post = PostTextBox.Text;
                DB.SaveChanges();
                MainDataGrid.ItemsSource = DB.Wages.ToList();
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }
            MessageBox.Show(message);
        }

        private void DeleteBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (MainDataGrid.SelectedItem != null)
            {
                DB.Salary.Remove(MainDataGrid.SelectedItem as Salary);
                DB.SaveChanges();
            }

            MainDataGrid.ItemsSource = DB.Salary.ToList();
        }
    }
}