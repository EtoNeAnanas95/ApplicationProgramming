﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FirstPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для WagesPage.xaml
    /// </summary>
    public partial class WagesPage : Page
    {
        private readonly FirstPracticeEntities DB;

        public WagesPage(FirstPracticeEntities DB)
        {
            InitializeComponent();
            this.DB = DB;
            MainDataGrid.ItemsSource = DB.Wages.ToList();
            var idsList = DB.Salary.ToList();

            SalaryComboBox.ItemsSource = DB.Salary.ToList();
            SalaryComboBox.DisplayMemberPath = "salary1";
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                var w = new Wages();
                message = "You are add new wage";
                w.wage = Convert.ToInt32(WageTextBox.Text);
                w.salary_ID = (SalaryComboBox.SelectedItem as Salary).ID_salary;
                DB.Wages.Add(w);
                DB.SaveChanges();
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }

            MainDataGrid.ItemsSource = DB.Wages.ToList();
            
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                message = "You are add new wage";
                var selected = MainDataGrid.SelectedItem as Wages;
                selected.salary_ID = (SalaryComboBox.SelectedItem as Salary).ID_salary;
                selected.wage = Convert.ToInt32(WageTextBox.Text);
                DB.SaveChanges();
                MainDataGrid.ItemsSource = DB.Wages.ToList();
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }
            MessageBox.Show(message);
        }

        private void DeleteBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (MainDataGrid.SelectedItem != null)
            {
                DB.Wages.Remove(MainDataGrid.SelectedItem as Wages);
                DB.SaveChanges();
            }

            MainDataGrid.ItemsSource = DB.Wages.ToList();
        }
    }
}