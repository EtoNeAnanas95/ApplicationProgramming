﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace FirstPractice_EntityFramework
{
    /// <summary>
    ///     Логика взаимодействия для AccountingPage.xaml
    /// </summary>
    public partial class AccountingPage : Page
    {
        private readonly FirstPracticeEntities DB;

        public AccountingPage(FirstPracticeEntities DB)
        {
            InitializeComponent();
            this.DB = DB;
            MainDataGrid.ItemsSource = DB.Accounting.ToList();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                var a = new Accounting();
                a.wages_payment_date = DateTime.Parse(WageDate.Text);
                a.name = Name.Text;
                a.surname = Surname.Text;
                a.middle_name = Middlename.Text;
                a.wages_ID = DB.Wages.ToList().Last().ID_wages;
                message = "You are add new employee";
                DB.Accounting.Add(a);
                DB.SaveChanges();
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }

            MainDataGrid.ItemsSource = DB.Accounting.ToList();
            MessageBox.Show(message);
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                message = "You are edit employee";
                var selected = MainDataGrid.SelectedItem as Accounting;
                selected.wages_payment_date = Convert.ToDateTime(WageDate.Text);
                selected.name = Name.Text;
                selected.surname = Surname.Text;
                selected.middle_name = Middlename.Text;
                DB.SaveChanges();
                MainDataGrid.ItemsSource = DB.Wages.ToList();
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }
            MessageBox.Show(message);
        }

        private void DeleteBase_OnClick(object sender, RoutedEventArgs e)
        {
            if (MainDataGrid.SelectedItem != null)
            {
                DB.Accounting.Remove(MainDataGrid.SelectedItem as Accounting);
                DB.SaveChanges();
            }

            MainDataGrid.ItemsSource = DB.Accounting.ToList();
        }
    }
}