﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using FirstPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace FirstPractice_DataSet
{
    public partial class Wages : Page
    {
        private readonly SalaryTableAdapter _salary;
        private readonly WagesTableAdapter _wages;

        public Wages(WagesTableAdapter _wages, SalaryTableAdapter _salary)
        {
            InitializeComponent();
            this._wages = _wages;
            this._salary = _salary;
            MainDataGrid.ItemsSource = _wages.GetData();
            var a = _salary.GetData();
            for (var i = 0; i < a.Rows.Count; i++) SalaryComboBox.Items.Add(Convert.ToString(a.Rows[i]["salary"]));
        }
        
        private int ValueToId(string value)
        {
            var a = _salary.GetData();
            for (var i = 0; i < a.Rows.Count; i++)
            {
                //SalaryComboBox.Items.Add(Convert.ToString(a.Rows[i]["salary"]));
                if (a.Rows[i]["salary"].ToString() == value) return Convert.ToInt32(a.Rows[i]["ID_salary"]);
            }
            return -1;
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                _wages.InsertQuery(Convert.ToInt32(WageTextBox.Text), ValueToId(SalaryComboBox.Text));
                message = "You are add new wage";
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }

            MainDataGrid.ItemsSource = _wages.GetData();
            MessageBox.Show(message);
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            var id = (MainDataGrid.SelectedItem as DataRowView).Row[0];
            _wages.UpdateQuery(Convert.ToInt32(WageTextBox.Text), ValueToId(SalaryComboBox.Text),
                Convert.ToInt32(id));
            MainDataGrid.ItemsSource = _wages.GetData();
        }

        private void DeleteBase_OnClick(object sender, RoutedEventArgs e)
        {
            var id = (MainDataGrid.SelectedItem as DataRowView).Row[0];
            _wages.DeleteQuery(Convert.ToInt32(id));
            MainDataGrid.ItemsSource = _wages.GetData();
        }
    }
}