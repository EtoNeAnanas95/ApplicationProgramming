﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using FirstPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace FirstPractice_DataSet
{
    public partial class Salary : Page
    {
        private readonly SalaryTableAdapter _salary;

        public Salary(SalaryTableAdapter _salary)
        {
            InitializeComponent();
            this._salary = _salary;
            MainDataGrid.ItemsSource = _salary.GetData();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                _salary.InsertQuery(Convert.ToInt32(SalaryTextBox.Text), PostTextBox.Text);
                message = "You are add new salary";
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }

            MainDataGrid.ItemsSource = _salary.GetData();
            MessageBox.Show(message);
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            var id = (MainDataGrid.SelectedItem as DataRowView).Row[0];
            _salary.UpdateQuery(Convert.ToInt32(SalaryTextBox.Text), PostTextBox.Text, Convert.ToInt32(id));
            MainDataGrid.ItemsSource = _salary.GetData();
        }

        private void DeleteBase_OnClick(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = (MainDataGrid.SelectedItem as DataRowView).Row[0];
                _salary.DeleteQuery(Convert.ToInt32(id));
                MainDataGrid.ItemsSource = _salary.GetData();
            }
            catch (Exception exception)
            {
                MessageBox.Show("you can't delete foreign keys!");
            }
        }
    }
}