﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Controls;
using FirstPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace FirstPractice_DataSet
{
    public partial class Accounting : Page
    {
        private readonly AccountingTableAdapter _accounting;
        private readonly WagesTableAdapter _wages;

        public Accounting(AccountingTableAdapter _accounting, WagesTableAdapter _wages)
        {
            InitializeComponent();
            this._accounting = _accounting;
            this._wages = _wages;
            MainDataGrid.ItemsSource = _accounting.GetData();
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            string message;
            try
            {
                _accounting.InsertQuery(WageDate.Text, Name.Text, Surname.Text, Middlename.Text,
                    Convert.ToInt32(_wages.GetData().Rows[_wages.GetData().Rows.Count - 1]["ID_wages"]));
                message = "You are add new employee";
            }
            catch (Exception exception)
            {
                message = "Incorrect data";
            }

            MainDataGrid.ItemsSource = _accounting.GetData();
            MessageBox.Show(message);
        }

        private void EditButton_OnClick(object sender, RoutedEventArgs e)
        {
            var id = (MainDataGrid.SelectedItem as DataRowView).Row[0];
            var wage_id = (MainDataGrid.SelectedItem as DataRowView).Row[5];
            _accounting.UpdateQuery(WageDate.Text, Name.Text, Surname.Text, Middlename.Text, Convert.ToInt32(wage_id),
                Convert.ToInt32(id));
            MainDataGrid.ItemsSource = _accounting.GetData();
        }

        private void DeleteBase_OnClick(object sender, RoutedEventArgs e)
        {
            var id = (MainDataGrid.SelectedItem as DataRowView).Row[0];
            _accounting.DeleteQuery(Convert.ToInt32(id));
            MainDataGrid.ItemsSource = _accounting.GetData();
        }
    }
}