//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан по шаблону.
//
//     Изменения, вносимые в этот файл вручную, могут привести к непредвиденной работе приложения.
//     Изменения, вносимые в этот файл вручную, будут перезаписаны при повторном создании кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ThirdPractice_EntityFramework
{
    using System;
    using System.Collections.Generic;
    
    public partial class Wages
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Wages()
        {
            this.Accounting = new HashSet<Accounting>();
        }
    
        public int ID_wages { get; set; }
        public Nullable<int> wage { get; set; }
        public Nullable<int> salary_ID { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Accounting> Accounting { get; set; }
        public virtual Salary Salary { get; set; }
    }
}
