﻿using System.Linq;
using System.Windows.Controls;

namespace ThirdPractice_EntityFramework
{
    public partial class MainPage : Page
    {
        public MainPage(FirstPracticeEntities1 DB)
        {
            InitializeComponent();
            MainGrid.ItemsSource = DB.Accounting.ToList();
        }
    }
}