﻿using System.Windows;
using System.Windows.Controls;
using ThirdPractice_DataSet.FirstPracticeDataSetTableAdapters;

namespace ThirdPractice_DataSet
{
    public partial class MainPage : Page
    {
        private AccountingTableAdapter _accounting = new AccountingTableAdapter();
        public MainPage()
        {
            InitializeComponent();
            MainGrid.ItemsSource = _accounting.GetFullInfo();
        }

        private void MainPage_OnLoaded(object sender, RoutedEventArgs e)
        {
            MainGrid.Columns[0].Visibility = Visibility.Hidden;
            MainGrid.Columns[5].Visibility = Visibility.Hidden;
            MainGrid.Columns[6].Visibility = Visibility.Hidden;
            MainGrid.Columns[8].Visibility = Visibility.Hidden;
            MainGrid.Columns[9].Visibility = Visibility.Hidden;

            MainGrid.Columns[1].Header = "Дата выдачи зп";
            MainGrid.Columns[2].Header = "Имя";
            MainGrid.Columns[3].Header = "Фамиллия";
            MainGrid.Columns[4].Header = "Отчество";
            MainGrid.Columns[7].Header = "ЗП";
            MainGrid.Columns[10].Header = "Оклад";
            MainGrid.Columns[11].Header = "Должность";
        }
    }
}